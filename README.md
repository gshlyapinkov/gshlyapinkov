# Grigoriy Shlyapinkovs Readme

## Personality 

### Meyer's Briggs

- **🤹 Personality**: [ESFP-T](https://www.16personalities.com/estj-personality  

- **🌋 Role** : An Executive (ESTJ) is someone with the Extraverted, Observant, Thinking, and Judging personality traits. They possess great fortitude, emphatically following their own sensible judgment. They often serve as a stabilizing force among others, able to offer solid direction amid adversity.

Traits: Extraverted – 78%, Observant – 54%, Thinking – 69%, Judging – 89%, Assertive – 69%



## Values

GitLab's [CREDIT](https://handbook.gitlab.com/handbook/values/#credit) Values is a big part of why I joined.

If I do need to chose any of the Values that would define me it would be mainly:

- Collaboration: I like working together and joining forces to reach our Goal. *Together* with my coworkers, customers and prospects **and** the wider GitLab community.
- Transparency: public by default. To the very extreme... well perhaps not on Instagram ;-) 

- iteration: Doing Small chunks of work and not having to wait for approvals, i absolutey love it!

## Communication Style

Communication is key to success of anything that involves more then 1 person working on it. Therefore I have multiple ways that I like to communicate:

- Quick Questions go to Slack, preferably in a public channel
- Personal, or privacy sensitive questions can of course go in DM or be discussed during meetings
- Product related changes/questions : Issue first wherever possible
- When in doubt, overcommunicate!

Keep in mind that English is my secondary language, so some subtle references might escape me. If it does, be direct about it. [Radical Candor](https://en.wikipedia.org/wiki/Radical_Candor) works for me, it's even preferred.

That also means feedback is welcome, feel free to tell me directly, or send [360 feedback](https://about.gitlab.com/handbook/business-technology/tech-stack/#cultureamp) my way through CultureAmp.

### ☕ Coffee Chats

From my first day at GitLab, until now, I'm part of the [#donut-be-strangers](https://about.gitlab.com/company/culture/all-remote/informal-communication/#the-donut-bot)-channel.

But if we aren't matched there, feel free to [schedule a coffee chat](https://app.reclaim.ai/m/kristof/coffee-chat) directly, my calendar is open.

## Events

### Bio

Grigoriy  is Collaboration Driven!  From his background in Techical Support he appreciates Collaboration, he knows that there are no problems, only challenges, and 'unexplained features'. 

 Outside of work, you'll find me entertaining my kid,flying to see the world, doing sports and  volunteering for animal causes, as well as play Dota2.


## Get in touch

- 🔗 LinkedIn: https://www.linkedin.com/in/gshlyapinkov/
- 🦊 Gitlab.com: http://gitlab.com/gshlyapinkov (⚠️ spoiler alert: you are here!)
